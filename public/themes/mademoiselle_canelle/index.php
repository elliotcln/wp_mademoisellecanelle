<?php get_header(); ?>

<main role="main" id="page-content">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <?php if (has_post_thumbnail()) : ?>
                <div class="page-hero">
                    <?php the_post_thumbnail(null, ['class' => 'page-hero__img']); ?>
                    <div class="container flex justify-center flex-col relative h-full">
                        <div class="absolute top-0"><?php get_breadcrumb('light', get_the_ID()); ?></div>
                        <?php the_title('<h1 class="text-center">', '</h1>'); ?>
                    </div>
                </div>
            <?php else : ?>
                <div class="container">
                    <?php get_breadcrumb(null, get_the_ID()); ?>
                    <?php the_title('<h1>', '</h1>'); ?>
                </div>
            <?php endif; ?>

            <div class="container py-6 lg:py-9 page-content">
                <?php the_content(); ?>
            </div>

        <?php endwhile; ?>
    <?php endif; ?>
</main>

<?php get_footer();
