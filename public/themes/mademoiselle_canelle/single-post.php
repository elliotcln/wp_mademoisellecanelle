<?php get_header(); ?>

<main role="main" id="page-content">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="container">
                <?php get_breadcrumb(null, get_the_ID()); ?>
                <div class="lg:flex lg:justify-between items-center">
                    <?php the_title('<h1>', '</h1>'); ?>
                    <button class="button print-button inline-flex place-items-center space-x-2 font-serif" OnClick="setTimeout(() => {javascript:window.print()}, 500)">
                        <svg role="img" class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                            <path fill="currentColor" d="M6 19H3a1 1 0 0 1-1-1V8a1 1 0 0 1 1-1h3V3a1 1 0 0 1 1-1h10a1 1 0 0 1 1 1v4h3a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1h-3v2a1 1 0 0 1-1 1H7a1 1 0 0 1-1-1v-2zm0-2v-1a1 1 0 0 1 1-1h10a1 1 0 0 1 1 1v1h2V9H4v8h2zM8 4v3h8V4H8zm0 13v3h8v-3H8zm-3-7h3v2H5v-2z" />
                        </svg>
                        <span>Imprimer cette fiche</span>
                    </button>
                </div>
            </div>

            <div class="container py-6 lg:py-9 page-content">
                <?php if (has_post_thumbnail()) : ?>
                    <div class="grid md:grid-cols-2 lg:grid-cols-3 gap-6">
                        <div class="print:hidden">
                            <?php the_post_thumbnail('thumbnail', ['class' => 'w-full rounded']); ?>
                        </div>
                        <div class="lg:col-span-2 print:max-w-xl print:mx-auto">
                            <?php the_content(); ?>
                        </div>
                    </div>
                <?php else : ?>
                    <div class="print:max-w-xl print:mx-auto">
                        <?php the_content(); ?>
                    </div>
                <?php endif; ?>
            </div>

        <?php endwhile; ?>
    <?php endif; ?>
</main>

<?php get_footer();
