<?php get_header(); ?>

<main role="main" id="page-content">
    <?php if (is_product_category()) : ?>
        <?php
        $term = get_queried_object();
        $cat_thumb_id = get_term_meta($term->term_id, 'thumbnail_id', true);
        $thumbnail = wp_get_attachment_url($cat_thumb_id);

        if ($cat_thumb_id != 0) :
        ?>
            <!-- if thumbnail exists -->
            <header class="absolute top-0 page-hero page-hero--transparent">
                <div class="hidden lg:block absolute right-0 lg:w-1/2 bg-caribbean opacity-20 h-96"></div>
                <div class="page-hero lg:w-4/6">
                    <img src="<?= $thumbnail ?>" class="page-hero__img" alt="">
                </div>

                <div class="absolute w-full left-0 bottom-0 pb-5 h-full flex flex-col justify-end">
                    <div class="container">
                        <div class="absolute top-0">
                            <?php
                            woocommerce_breadcrumb([
                                'home' => 'Mademoiselle Canelle',
                                'delimiter' => null,
                                'before' => '<li>',
                                'after' => '</li>',
                                'wrap_before' => '<ul class="breadcrumb is-light">',
                                'wrap_after' => '</ul>'
                            ]);
                            ?>
                        </div>
                        <h1><?php woocommerce_page_title(); ?></h1>
                    </div>
                </div>
            </header>
            <div class="font-serif bg-caribbean bg-opacity-20 lg:bg-transparent">
                <div class="container">
                    <div class="lg:w-1/2 p-4 lg:pl-0 lg:pr-4">
                        <?php do_action('woocommerce_archive_description'); ?>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <!-- no category thumbnail -->
            <div class="container">
                <?php
                woocommerce_breadcrumb([
                    'home' => 'Mademoiselle Canelle',
                    'delimiter' => null,
                    'before' => '<li>',
                    'after' => '</li>',
                    'wrap_before' => '<ul class="breadcrumb">',
                    'wrap_after' => '</ul>'
                ]);
                ?>
                <h1><?php woocommerce_page_title(); ?></h1>
            </div>
        <?php endif; ?>

        <!-- category content - display items or subcategory -->
        <div class="container py-8">
            <!-- display children categories -->
            <?php
            $children = get_terms($term->taxonomy, array(
                'parent'    => $term->term_id,
                'hide_empty' => false
            ));
            if ($children) :
            ?>
                <div class="flex flex-col md:flex-row justify-center border-t border-b mb-8 lg:mb-12 border-gray-200 divide-y md:divide-y-0 md:divide-x text-sm opacity-70">
                    <?php
                    foreach ($children as $child) {
                        $meta = get_term_meta($child->term_id);
                        $element_count = '0';
                        if (isset($meta['product_count_product_cat'])) {
                            $element_count = $meta['product_count_product_cat'][0];
                        }
                        echo '<a class="block text-center p-4 hover:underline hover:text-caribbean" href="' . $child->slug . '">' . $child->name . ' (' . $element_count . ')</a>';
                    }
                    ?>
                </div>
            <?php endif; ?>

            <?= do_shortcode('[shop_messages]'); ?>

            <?php
            woocommerce_product_loop_start();
            $args = array(
                'post_type' => 'product',
                'tax_query' => array(
                    array(
                        'taxonomy'      => 'product_cat',
                        'terms'         => $term->term_id,
                        'field'         => 'id',
                        'operator'      => 'IN'
                    )
                )
            );
            $loop = new WP_Query($args);
            if ($loop->have_posts()) {
                while ($loop->have_posts()) : $loop->the_post();
                    wc_get_template_part('content', 'product');
                endwhile;
            } else {
                echo '<div class="col-span-2 md:col-span-3 lg:col-span-4">';
                get_template_part('content', 'empty-shop');
                echo '</div>';
            }
            wp_reset_postdata();
            woocommerce_product_loop_end();
            ?>
        </div>

    <?php else : ?>
        <div class="pb-8">
            <div class="container">
                <?php
                woocommerce_breadcrumb([
                    'home' => 'Mademoiselle Canelle',
                    'delimiter' => null,
                    'before' => '<li>',
                    'after' => '</li>',
                    'wrap_before' => '<ul class="breadcrumb">',
                    'wrap_after' => '</ul>'
                ]);
                ?>
            </div>
            <?php
            woocommerce_content();
            ?>
        </div>
    <?php endif; ?>
</main>

<?php get_footer();
