<section class="py-8 lg:py-12 bg-ghost cta-footer overflow-hidden">
  <div class="container relative md:flex space-y-4 md:space-y-0 items-center justify-between">
    <div>
      <h2>Une envie spécifique ?</h2>
      <p>N'hésitez pas à nous contacter.</p>
    </div>
    <?php
    $phone = $_ENV['PHONE_NUMBER'];
    $mail = $_ENV['MAIL'];
    ?>
    <div class="flex space-x-2">
      <a href="mailto:<?= $mail; ?>" class="button proceed w-full md:w-auto">Par e-mail</a>
      <a href="tel:<?= $phone; ?>" class="button proceed w-full md:w-auto">Par téléphone</a>
    </div>
  </div>
</section>

<!-- <section class="py-8 lg:py-12 text-sm border-b border-t">
    <div class="container">
        <div class="lg:w-1/2 contact-form--footer">
            <?= do_shortcode('[contact-form-7 id="321" title="Footer contact"]'); ?>
        </div>
    </div>
</section> -->