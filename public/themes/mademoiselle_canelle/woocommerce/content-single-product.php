<?php

/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

<div id="product-<?php the_ID(); ?>" <?php wc_product_class('', $product); ?>>
	<div class="container">
		<header class="px-4 border-l-2 border-caribbean">
			<div class="flex items-baseline space-x-4 flex-wrap">
				<?php woocommerce_template_single_title(); ?>
				<?php woocommerce_template_single_price(); ?>

			</div>
			<?php if ($product->is_featured()) : ?>
				<div class="inline-flex text-red-500 font-bold text-xs tracking-wide space-x-2">
					<svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
						<path fill="currentColor" d="M16.5 3C19.538 3 22 5.5 22 9c0 7-7.5 11-10 12.5C9.5 20 2 16 2 9c0-3.5 2.5-6 5.5-6C9.36 3 11 4 12 5c1-1 2.64-2 4.5-2z" />
					</svg>
					<span>Coup de coeur</span>
				</div>
			<?php endif; ?>
			<div class="font-serif max-w-xl"><?php the_content(); ?></div>
		</header>

		<section class="grid md:grid-flow-col-dense items-start md:grid-cols-3 gap-4 py-6">
			<!-- get product thumbnails -->
			<div class="md:col-span-1 p-4 rounded border relative">
				<?php do_action('woocommerce_before_single_product_summary'); ?>
			</div>
			<!-- get product infos -->
			<div class="md:col-span-2  border p-4 rounded">
				<?php woocommerce_template_single_add_to_cart(); ?>

				<hr class="border-0 my-4 border-b">
				<div>
					<?php

					$attributes = $product->get_attributes();
					if ($attributes) :
					?>
						<div class="text-sm"><?php echo wc_display_product_attributes($product); ?></div>
						<hr class="border-0 my-4 border-b">
					<?php endif; ?>
				</div>
				<?php woocommerce_template_single_meta(); ?>
				<div class="share pt-4 border-t mt-4">
					<?php woocommerce_template_single_sharing(); ?>
				</div>
			</div>
		</section>
	</div>

	<section class="space-y-4 py-4 lg:py-8 bg-caribbean bg-opacity-5">
		<?php
		woocommerce_upsell_display();
		woocommerce_output_related_products();
		?>
	</section>

</div>

<?php do_action('woocommerce_after_single_product'); ?>