<?php

/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

// Ensure visibility.
if (empty($product) || !$product->is_visible()) {
	return;
}
?>
<div>

	<!-- loop product -->
	<div class="group wc-product relative">
		<div class="cat-thumb relative">
			<a class="absolute inset-0 lg:hidden" href="<?php the_permalink(); ?>" aria-label="Voir le détail de <?php the_title(); ?>">&nbsp;</a>
			<?php do_action('woocommerce_before_shop_loop_item_title'); ?>
			<?php
			if ($product->is_featured()) {
				wc_featured_product($product);
			}
			?>
			<?php do_action('woocommerce_after_shop_loop_item'); ?>
		</div>
		<!-- Product title & price -->
		<a href="<?php the_permalink(); ?>" class="block my-2 group-hover:underline">
			<h2 class="font-serif text-xl lg:text-2xl"><?php the_title(); ?></h2>
			<?php do_action('woocommerce_after_shop_loop_item_title'); ?>
		</a>
	</div>
</div>