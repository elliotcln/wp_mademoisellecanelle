<?php

/**
 * Register & load scripts & styles
 */
add_action('wp_enqueue_scripts', function () {
    wp_register_style('fonts', 'https://fonts.googleapis.com/css2?family=Inria+Serif:wght@400;700&family=Inter:wght@400;700&family=Encode+Sans:wght@400;700&display=swap');
    wp_register_style('style', get_template_directory_uri() . '/style.css');
    wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js', array(), '1.0.0');


    // enqueue
    wp_enqueue_style('fonts');
    wp_enqueue_style('style');
    wp_enqueue_script('jquery');

    wp_dequeue_style('wc-block-style');
});

/**
 * Set theme supports
 */
add_action('after_setup_theme', function () {
    add_theme_support('post-thumbnails');
    add_theme_support('woocommerce');
    add_theme_support('wc-product-gallery-zoom');
    add_theme_support('wc-product-gallery-lightbox');
    add_theme_support('wc-product-gallery-slider');

    add_theme_support('title-tag');
    add_theme_support( 'custom-logo' );

    register_nav_menus([
        'navigation' => __('Navigation'),
        'footer' => __('Footer'),
    ]);
});

function wp_remove_admin_links()
{
    // remove_menu_page('complianz');
    remove_menu_page('elementor');
    remove_menu_page('wpfastestcacheoptions');
    // remove_menu_page('edit.php');
    remove_menu_page('edit.php?post_type=acf-field-group');
    remove_menu_page('edit-comments.php');
    // remove_menu_page('themes.php');
    remove_menu_page('edit.php?post_type=elementor_library');
}
add_action('admin_init', 'wp_remove_admin_links');


/**
 * Improve jpeg quality to 100
 */
add_filter('jpeg_quality', function () {
    return 100;
}, 10, 2);

/**
 * Woocommerce - My account menu
 */
add_filter('woocommerce_account_menu_items', function ($items) {
    unset($items['downloads']);
    return $items;
}, 99, 1);

// remove basic product filter
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

/**
 * Woocommerce - Empty cart
 */
remove_action('woocommerce_cart_is_empty', 'wc_empty_cart_message', 10);
add_action('woocommerce_cart_is_empty', 'custom_empty_cart_message', 10);

function custom_empty_cart_message()
{
    $html  = '<div class="mc-alert mc-alert--notice"><p class="cart-empty">';
    $html .= wp_kses_post(apply_filters('wc_empty_cart_message', __('Your cart is currently empty.', 'woocommerce')));
    echo $html . '</p></div>';
}

/**
 * Featured product
 */
function wc_featured_product($product)
{
    $ft = '<div class="z-40 absolute top-2 right-2 bg-ghost inline-flex text-red-500 place-items-center rounded-full p-2 text-xs font-bold tracking-wide space-x-2">';
    $ft .= '<svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="currentColor" d="M16.5 3C19.538 3 22 5.5 22 9c0 7-7.5 11-10 12.5C9.5 20 2 16 2 9c0-3.5 2.5-6 5.5-6C9.36 3 11 4 12 5c1-1 2.64-2 4.5-2z"/></svg>';
    $ft .= '<span>Coup de coeur</span>';
    $ft .= '</div>';

    echo $ft;
}

remove_action('woocommerce_shop_loop_subcategory_title', 'woocommerce_template_loop_category_title', 10);
add_action('woocommerce_shop_loop_subcategory_title', 'custom_woocommerce_template_loop_category_title', 10);
function custom_woocommerce_template_loop_category_title($category)
{
?>
    <h2 class="text-center w-full rounded p-2 bg-charcoal text-ghost"><?= $category->name; ?></h2>
    <?php
}


function mc_wc_get_gallery_image_html($attachment_id, $main_image = false)
{
    $flexslider        = (bool) apply_filters('woocommerce_single_product_flexslider_enabled', get_theme_support('wc-product-gallery-slider'));
    $gallery_thumbnail = wc_get_image_size('gallery_thumbnail');
    $thumbnail_size    = apply_filters('woocommerce_gallery_thumbnail_size', array($gallery_thumbnail['width'], $gallery_thumbnail['height']));
    $image_size        = apply_filters('woocommerce_gallery_image_size', $flexslider || $main_image ? 'woocommerce_thumbnail' : $thumbnail_size);
    $full_size         = apply_filters('woocommerce_gallery_full_size', apply_filters('woocommerce_product_thumbnails_large_size', 'full'));
    $thumbnail_src     = wp_get_attachment_image_src($attachment_id, $thumbnail_size);
    $full_src          = wp_get_attachment_image_src($attachment_id, $full_size);
    $alt_text          = trim(wp_strip_all_tags(get_post_meta($attachment_id, '_wp_attachment_image_alt', true)));
    $image             = wp_get_attachment_image(
        $attachment_id,
        $image_size,
        false,
        apply_filters(
            'woocommerce_gallery_image_html_attachment_image_params',
            array(
                'title'                   => _wp_specialchars(get_post_field('post_title', $attachment_id), ENT_QUOTES, 'UTF-8', true),
                'data-caption'            => _wp_specialchars(get_post_field('post_excerpt', $attachment_id), ENT_QUOTES, 'UTF-8', true),
                'data-src'                => esc_url($full_src[0]),
                'data-large_image'        => esc_url($full_src[0]),
                'data-large_image_width'  => esc_attr($full_src[1]),
                'data-large_image_height' => esc_attr($full_src[2]),
                'class'                   => esc_attr($main_image ? 'wp-post-image' : ''),
            ),
            $attachment_id,
            $image_size,
            $main_image
        )
    );

    return '<div data-thumb="' . esc_url($thumbnail_src[0]) . '" data-thumb-alt="' . esc_attr($alt_text) . '" class="woocommerce-product-gallery__image"><a href="' . esc_url($full_src[0]) . '">' . $image . '</a></div>';
}


/**
 * Undocumented function
 *
 * @param [string] $code
 * @return void
 */
function get_offer_banner($code)
{
    // global $woocommerce;
    $c = new WC_Coupon($code);
    // echo $c;
    $c_data = array(
        'id' => $c->get_id(),
        'description' => $c->get_description(),
        'amount' => wc_format_decimal($c->get_amount(), 2),
        'expiration_date' => $c->get_date_expires()
    );

    $coupon = array('coupon' => apply_filters('woocommerce_api_coupon_response', $c_data, $c));

    $banner = '';
    $current_time = date('Y-m-d');
    if (isset($coupon['coupon']['expiration_date']) && $current_time > $coupon['coupon']['expiration_date']) {
        return false;
    }
    if (isset($coupon) && $c_data['id'] !== 0) {
        $banner .= '<div class="offer-banner print:hidden px-2 text-center flex flex-wrap py-3 bg-charcoal space-x-4 place-items-center justify-center text-ghost text-xs font-serif">';
        $banner .= '<p><strong class="font-sans">' . $coupon['coupon']['description'] . ' :</strong> ';
        $banner .= '-' . $c->get_amount();
        if ($c->get_discount_type() === 'percent') {
            $banner .= '%';
        } else {
            $banner .= ' euros';
        }
        $banner .= ' avec le code <span class="uppercase">"' . $c->get_code() . '"</span></p>';
        $banner .= '<span class="hidden sm:block w-12 border-b border-ghost opacity-30"></span><a href="' . wc_get_page_permalink('shop') . '" class=" text-caribbean flex place-items-center space-x-1">Voir la boutique <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path/><path fill="currentColor" d="M12.172 12L9.343 9.172l1.414-1.415L15 12l-4.243 4.243-1.414-1.415z"/></svg></a>';
        $banner .= '</div>';
    }

    echo $banner;
}

/**
 * Define single walker footer navigation
 */
class Footer_Walker extends Walker_Nav_Menu
{

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        $attributes  = !empty($item->attr_title) ? ' title="'  . esc_attr($item->attr_title) . '"' : '';
        $attributes .= !empty($item->target)     ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= !empty($item->xfn)        ? ' rel="'    . esc_attr($item->xfn) . '"' : '';
        $attributes .= !empty($item->url)        ? ' href="'   . esc_attr($item->url) . '"' : '';
        echo '<a ' . $attributes . ' >' . apply_filters('the_title', $item->title, $item->ID) . '</a>';
    }
}


/**
 * Define navigation walker
 */
class Main_Nav_Walker extends Walker_Nav_Menu
{

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        $indent = ($depth) ? str_repeat("\t", $depth) : '';

        $class_names = $value = '';

        $classes = empty($item->classes) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;

        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
        if ($args->walker->has_children) {
            $class_names .= ' group';
        }
        $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';

        $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
        $id = $id ? ' id="' . esc_attr($id) . '"' : '';

        $output .= $indent . '';

        $attributes  = !empty($item->attr_title) ? ' title="'  . esc_attr($item->attr_title) . '"' : '';
        $attributes .= !empty($item->target)     ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= !empty($item->xfn)        ? ' rel="'    . esc_attr($item->xfn) . '"' : '';
        $attributes .= !empty($item->url)        ? ' href="'   . esc_attr($item->url) . '"' : '';
        if (in_array('current-menu-item', $item->classes)) {
            $attributes .= 'aria-current="page"';
        }

        $item_output = $args->before;
        $item_output .= '<li' . $class_names . '>';
        $item_output .= '<a' . $attributes . ' class="flex-grow">';
        $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
        $item_output .= '</a>';
        if ($args->walker->has_children) {
            $item_output .= '<label role="button" for="check-menu-item-parent-' . $item->ID . '" class="ml-auto lg:ml-1 w-7 h-7 flex place-items-center justify-center">
                <svg fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 15l-4.243-4.243 1.415-1.414L12 12.172l2.828-2.829 1.415 1.414z"/></svg>
            </label>';
        }
        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
        $item_output .= $args->after;
        if ($args->walker->has_children) {
            $output .= '<input type="checkbox" class="hidden" id="check-menu-item-parent-' . $item->ID . '">';
        }
        $item_output .= '</li>';
    }
}

/**
 * Get the breadcrumb
 *
 * @param string $theme
 * @return breadcrumb
 */
function get_breadcrumb($theme = 'dark', $post_ID)
{

    $bread = '';
    $bread_classes = 'breadcrumb';
    if ($theme === 'light') {
        $bread_classes .= ' is-light';
    }


    $bread .= '<ul class="' . $bread_classes . '">';
    $attrs = '';
    if (get_permalink(get_the_ID()) === get_the_permalink()) {
        $attrs .= 'aria-current="page"';
    }
    if (is_page() || is_single()) {
        $bread .= '<li><a href="' . get_bloginfo('url') . '">' . get_bloginfo('title') . '</a></li>';
        if (get_post($post_ID)->post_parent) {
            $bread .= '<li><a href="' . get_permalink(get_post($post_ID)->post_parent) . '">' . get_the_title(get_post($post_ID)->post_parent) . '</a></li>';
        }
        if (get_the_category($post_ID)) {

            $cat = get_the_category($post_ID)[0];
            $cat_url = get_category_link($cat);
            $bread .= '<li><a href="' . $cat_url . '">' . $cat->name . '</a></li>';
        }
        $bread .= '<li ' . $attrs . '>' . get_the_title() . '</li>';
    } else if (is_category()) {
        $bread .= '<li><a href="' . get_bloginfo('url') . '">' . get_bloginfo('title') . '</a></li>';
        $cat = get_the_category()[0];
        $bread .= '<li ' . $attrs . '>' . $cat->name . '</li>';
    }
    $bread .= '</ul>';

    echo $bread;
}

add_action('woocommerce_share', 'crunchify_social_sharing_buttons');
function crunchify_social_sharing_buttons($content)
{
    if (is_singular() || is_home()) {

        // Get current page URL 
        $url = get_permalink();

        // Get current page title
        $title = str_replace(' ', '%20', get_the_title());

        // Get Post Thumbnail for pinterest

        // Construct sharing URL without using any script
        $twitterURL = 'https://twitter.com/intent/tweet?text=' . $title . '&amp;url=' . $url;
        $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u=' . $url;
        $googleURL = 'https://plus.google.com/share?url=' . $url;

        // Add sharing buttoxn at the end of page/page content
        $content .= '<div class="crunchify-social">';
        $content .= '<h5>Partager sur:</h5>';
        $content .= '<div class="flex space-x-4 mt-1">';
        $content .= '<a class="text-sm text-charcoal-light hover:text-caribbean hover:underline" href="' . $twitterURL . '" target="_blank">Twitter</a>';
        $content .= '<a class="text-sm text-charcoal-light hover:text-caribbean hover:underline" href="' . $facebookURL . '" target="_blank">Facebook</a>';
        $content .= '<a class="text-sm text-charcoal-light hover:text-caribbean hover:underline" href="' . $googleURL . '" target="_blank">Google+</a>';
        $content .= '</div>';
        $content .= '</div>';

        echo $content;
    }
}


function woocommerce_content()
{

    if (is_singular('product')) {

        while (have_posts()) :
            the_post();
            wc_get_template_part('content', 'single-product');
        endwhile;
    } else {
    ?>
        <div class="container">
            <?php if (apply_filters('woocommerce_show_page_title', true)) : ?>
                <h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

            <?php endif; ?>

            <?php do_action('woocommerce_archive_description'); ?>

            <?php if (woocommerce_product_loop()) : ?>

                <?php do_action('woocommerce_before_shop_loop'); ?>
                <?php woocommerce_product_loop_start(); ?>

                <?php if (wc_get_loop_prop('total')) : ?>
                    <?php while (have_posts()) : ?>
                        <?php the_post(); ?>
                        <?php wc_get_template_part('content', 'product'); ?>
                    <?php endwhile; ?>
                <?php endif; ?>

                <?php woocommerce_product_loop_end(); ?>

                <?php do_action('woocommerce_after_shop_loop'); ?>
        </div>
<?php
            else :
                do_action('woocommerce_no_products_found');
            endif;
        }
    }
