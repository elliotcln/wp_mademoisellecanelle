<?php get_header(); ?>

<main role="main" id="page-content">
    <section class="relative py-24 lg:py-32 xl:py-36 overflow-hidden bg-gradient-to-br from-charcoal to-charcoal-dark text-ghost flex place-items-center">
        <?php
        $hero_el = get_field('homepage_hero');
        $thumb = $hero_el['thumbnail'];
        $title = $hero_el['title'];
        $desc = $hero_el['desc'];
        $btn_link = $hero_el['btn_link'];

        if (!empty($thumb)) :
            echo wp_get_attachment_image($thumb['id'], null, false, array(
                'class' => 'absolute z-1 inset-0 object-cover h-full w-full opacity-70'
            ));
        ?>
        <?php endif; ?>
        <!-- <div class="custom-shape-divider-bottom-1613638154 absolute inset-x-0 bottom-0 transform rotate-180">
            <svg class="h-5 w-full text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
                <path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z" fill="currentColor"></path>
            </svg>
        </div> -->
        <div class="container relative">
            <div class="max-w-2xl text-center mx-auto">
                <h1><?= $title; ?></h1>
                <?php if (!empty($desc)) : ?>
                    <p><?= $desc; ?></p>
                <?php endif; ?>
                <?php if (!empty($btn_link)) : ?>
                    <a href="<?= $btn_link['url'] ?>" class="button proceed inline-block mt-4"><?= $btn_link['title']; ?></a>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <section class="py-8 lg:py-12 bg-caribbean bg-opacity-5">
        <div class="container">
            <div class="grid md:grid-cols-3 divide-y md:divide-x md:divide-y-0">
                <div class="text-center p-4">
                    <img class="h-20 inline-block" src="<?= get_template_directory_uri(); ?>/assets/images/icons/rose-02.svg" alt="">
                    <h2 class="mt-4">Des fleurs
                        <small class="block text-caribbean font-serif font-normal">Séchées & Fraîches</small>
                    </h2>
                </div>
                <div class="text-center p-4">
                    <img class="h-20 inline-block" src="<?= get_template_directory_uri(); ?>/assets/images/icons/plant-02.svg" alt="">
                    <h2 class="mt-4">Des plantes
                        <small class="block text-caribbean font-serif font-normal">Grasses & Aromatiques</small>
                    </h2>
                </div>
                <div class="text-center p-4">
                    <img class="h-20 inline-block" src="<?= get_template_directory_uri(); ?>/assets/images/icons/watering-can-02.svg" alt="">
                    <h2 class="mt-4">De la décoration
                        <small class="block text-caribbean font-serif font-normal">La touche "en plus"</small>
                    </h2>
                </div>
            </div>
            <div class="text-center mt-8 lg:mt-12">
                <a href="<?= wc_get_page_permalink('shop'); ?>" class="button">Découvrir la boutique</a>
            </div>
        </div>
    </section>

    <section class="py-12 lg:py-20">
        <div class="container">
            <div class="md:flex md:space-x-12 space-y-8 md:space-y-0 items-center justify-center">
                <img class="h-36" src="<?= get_template_directory_uri(); ?>/assets/images/icons/document-02.svg" alt="">
                <div>
                    <h2>Des fiches "Astuces & Conseils"</h2>
                    <p class="max-w-xl mx-auto mt-4">Ici vous trouverez toutes les petites astuces sur des plantes, mais aussi sur l'entretien de vos bouquets de fleurs et vos créations en fleurs séchées.</p>
                    <div class="mt-8">
                        <?php
                            $cat_id = get_cat_ID('Astuces & Conseils');
                            $cat_link = get_category_link($cat_id);
                        ?>
                        <a href="<?= $cat_link; ?>" class="button">Découvrir les Astuces & Conseils</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- <?php if (have_posts()) : ?>
        <div class="py-8 xl:mt-8">
            <div class="container">
                <?php //echo do_shortcode('[shop_messages]'); 
                ?>
            </div>
            <section class="woocommerce-section mb-8 lg:mb-12">
                <div class="container">
                    <h2 class="mb-4">Nos produits les plus vendus</h2>
                    <?php //echo do_shortcode('[best_selling_products limit="4"]'); 
                    ?>
                </div>
            </section>
            <section class="woocommerce-section bg-caribbean bg-opacity-5 py-4 lg:py-8">
                <div class="container">
                    <h2 class="mb-4">Nos produits coup de coeur</h2>
                    <?php //echo do_shortcode('[featured_products limit="4"]'); 
                    ?>
                </div>
            </section>
        </div>
    <?php endif; ?> -->
</main>

<?php get_footer();
