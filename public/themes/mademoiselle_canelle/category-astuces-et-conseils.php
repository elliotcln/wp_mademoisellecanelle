<?php get_header(); ?>

<main role="main" id="page-content">
    <!-- get Astuces & Conseils posts' category -->
    <?php
    $cat = get_the_category();
    $args = array('category' => $cat[0]->cat_ID);
    $posts = get_posts($args);
    ?>
    <div class="container">
        <?php get_breadcrumb(null, get_the_ID()); ?>
        <h1><?= $cat[0]->name; ?></h1>
        <?php if ($cat[0]->description) : ?>
            <div class="page-content text-gray-400 mt-4">
                <?= $cat[0]->description; ?>
            </div>
        <?php endif; ?>
    </div>

    <div class="container py-6 lg:py-9 page-content">
        <ul class=" divide-y">
            <?php foreach ($posts as $post) : setup_postdata($post); ?>
                <li>
                    <?php
                    $blocks = parse_blocks($post->post_content);
                    if ($blocks && $blocks[0]['blockName'] === 'core/file') {
                        $file_url = $blocks[0]['attrs']['href'];
                        echo '<a target="_blank" href="' . $file_url . '" class="border-l-4 hover:border-caribbean group p-4 flex place-items-center space-x-2 md:space-x-4">
                                        <div class="flex place-items-center space-x-2 font-serif text-lg">
                                            <span>' . get_the_title() . '</span>
                                            <svg xmlns="http://www.w3.org/2000/svg" class="w-4 h-4 text-gray-400 group-hover:text-caribbean" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0z"/><path fill="currentColor" d="M10 6v2H5v11h11v-5h2v6a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h6zm11-3v8h-2V6.413l-7.793 7.794-1.414-1.414L17.585 5H13V3h8z"/></svg>
                                        </div>
                                    </a>';
                    } else {
                        echo '<a href="' . get_the_permalink() . '" class="group border-l-4 hover:border-caribbean p-4 flex place-items-center space-x-2 md:space-x-4">
                                        <div class="flex place-items-center space-x-2 font-serif text-lg">
                                            <span>' . get_the_title() . '</span>
                                        </div>
                                    </a>';
                    }
                    ?>
                </li>
            <?php wp_reset_postdata();
            endforeach; ?>
        </ul>
    </div>
</main>

<?php get_footer();
