<div class=" space-y-4 mb-8">
    <p class="text-4xl font-bold">Whoops ...</p>
    <p class="text-lg text-gray-400"><?php esc_html_e('No products were found matching your selection.', 'woocommerce'); ?></p>
    <!-- <p class="text-lg text-gray-400">La boutique en ligne n'est pas encore disponible.<br/>N'hésitez pas à passer nous voir en boutique à Saint-Briac sur Mer.</p> -->
    <a href="<?= get_home_url(); ?>" class="button">Retourner sur l'accueil du site</a>
</div>