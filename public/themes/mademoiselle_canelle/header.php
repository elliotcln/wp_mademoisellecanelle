<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>

<body <?php body_class('text-charcoal'); ?>>
    <?php get_offer_banner('OPENING20'); ?>
    <header class="site-header mb-2">
        <div class="2xl:container">
            <!-- print header -->
            <div class="hidden print:block text-center">
                <p class="text-4xl font-bold"><?= bloginfo('title'); ?></p>
                <p class="text-xl text-gray-500"><?= bloginfo('description'); ?></p>
                <!-- <img src="<?= get_template_directory_uri(); ?>/assets/images/logo_print.jpg" alt="Mademoiselle Canelle - Logo"> -->
            </div>
            <!-- mobile header -->
            <div class="flex place-items-center justify-between px-5 lg:hidden print:hidden">
                <label for="toggle-main-navigation" role="button" class="w-7 h-7 flex place-items-center justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                        <path fill="none" d="M0 0h24v24H0z" />
                        <path fill="currentColor" d="M3 4h18v2H3V4zm0 7h18v2H3v-2zm0 7h18v2H3v-2z" />
                    </svg>
                </label>
                <a href="<?php bloginfo('url'); ?>" class="transform translate-x-6 mobile-logo-container">
                    <img class="logo--mobile" src="<?= get_template_directory_uri(); ?>/assets/images/logo_mademoiselle-canelle--dark.svg" alt="Mademoiselle Canelle - Logo">
                </a>
                <div class="w-14 flex space-x-2 ml-5 icons-nav">
                    <a href="<?= wc_get_cart_url(); ?>" class="hover:text-caribbean">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" width="24" height="24">
                            <path fill="none" d="M0 0h24v24H0z" />
                            <path d="M7 8V6a5 5 0 1 1 10 0v2h3a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1h3zm0 2H5v10h14V10h-2v2h-2v-2H9v2H7v-2zm2-2h6V6a3 3 0 0 0-6 0v2z" />
                        </svg>
                    </a>
                    <a href="<?= wc_get_page_permalink('myaccount') ?>" class="hover:text-caribbean">
                        <svg fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                            <path fill="none" d="M0 0h24v24H0z" />
                            <path d="M12 17c3.662 0 6.865 1.575 8.607 3.925l-1.842.871C17.347 20.116 14.847 19 12 19c-2.847 0-5.347 1.116-6.765 2.796l-1.841-.872C5.136 18.574 8.338 17 12 17zm0-15a5 5 0 0 1 5 5v3a5 5 0 0 1-4.783 4.995L12 15a5 5 0 0 1-5-5V7a5 5 0 0 1 4.783-4.995L12 2zm0 2a3 3 0 0 0-2.995 2.824L9 7v3a3 3 0 0 0 5.995.176L15 10V7a3 3 0 0 0-3-3z" />
                        </svg>
                    </a>
                </div>
            </div>

            <!-- main nav desktop -->
            <input type="checkbox" id="toggle-main-navigation" class="hidden">
            <nav class="lg:mt-3 px-5 main-navigation print:hidden" role="navigation" aria-label="Navigation Principale">
                <ul class="lg:text-sm xl:text-base lg:flex lg:space-x-2 lg:justify-around lg:items-center">
                    <?php wp_nav_menu([
                        'theme_location' => 'navigation',
                        'items_wrap' => '%3$s',
                        'container' => null,
                        'walker' => new Main_Nav_Walker()
                    ]); ?>
                    <li class="hidden lg:flex space-x-4">
                        <a href="<?= wc_get_cart_url(); ?>" class="hover:text-caribbean">
                            <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" width="24" height="24">
                                <path fill="none" d="M0 0h24v24H0z" />
                                <path d="M7 8V6a5 5 0 1 1 10 0v2h3a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1h3zm0 2H5v10h14V10h-2v2h-2v-2H9v2H7v-2zm2-2h6V6a3 3 0 0 0-6 0v2z" />
                            </svg>
                        </a>
                        <a href="<?= wc_get_page_permalink('myaccount') ?>" class="hover:text-caribbean">
                            <svg class="h-5 w-5" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24">
                                <path fill="none" d="M0 0h24v24H0z" />
                                <path d="M12 17c3.662 0 6.865 1.575 8.607 3.925l-1.842.871C17.347 20.116 14.847 19 12 19c-2.847 0-5.347 1.116-6.765 2.796l-1.841-.872C5.136 18.574 8.338 17 12 17zm0-15a5 5 0 0 1 5 5v3a5 5 0 0 1-4.783 4.995L12 15a5 5 0 0 1-5-5V7a5 5 0 0 1 4.783-4.995L12 2zm0 2a3 3 0 0 0-2.995 2.824L9 7v3a3 3 0 0 0 5.995.176L15 10V7a3 3 0 0 0-3-3z" />
                            </svg>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

        <!-- print separator -->
        <div class="w-1/3 mx-auto border-b hidden print:block mt-8 mb-4"></div>
    </header>