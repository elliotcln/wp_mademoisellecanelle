const theme = process.env.WP_THEME;

module.exports = {
  purge: [
    `./public/themes/${theme}/**/*.php`,
    `./resources/styles/**/*.scss`
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens: {
        'print': { 'raw': 'print' },
      },
      container: {
        center: true,
        padding: {
          DEFAULT: '1rem',
          '2xl': 0
        }
      },
      fontFamily: {
        sans: ['Encode Sans', 'sans-serif'],
        serif: ['Inria Serif', 'serif'],
      },
      colors: {
        charcoal: {
          light: '#a4b8cc',
          DEFAULT: '#2E4052',
          dark: '#161f27'
        },
        caribbean: {
          light: '#abd8cd',
          DEFAULT: '#58B39D',
          dark: '#275449'
        },
        ghost: '#FAFAFF'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
